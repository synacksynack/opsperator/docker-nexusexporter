FROM docker.io/python:3.7-alpine

# Nexus Exporter image for OpenShift Origin

LABEL io.k8s.description="Nexus Prometheus Exporter Image." \
      io.k8s.display-name="Nexus Prometheus Exporter" \
      io.openshift.expose-services="9183:http" \
      io.openshift.tags="prometheus,exporter,nexus" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-nexusexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="0.2.3"

WORKDIR /usr/src/app
COPY config/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY config/nexus_exporter.py /nexus_exporter.py
EXPOSE 9184
ENTRYPOINT ["/nexus_exporter.py"]
USER 1000
